-- Pokemon Table
INSERT INTO Pokemon (name, primary_type, secondary_type)
VALUES 
  ('Bulbasaur', 'Grass', NULL),
  ('Charmander', 'Fire', NULL),
  ('Squirtle', 'Water', NULL),
  ('Eevee', 'Normal', NULL),
  ('Pidgey', 'Normal', 'Flying');

-- Type Table
INSERT INTO Type (name)
VALUES 
  ('Grass'),
  ('Fire'),
  ('Water'),
  ('Normal'),
  ('Flying');

-- Move Table
INSERT INTO Move (name, power, type_id)
VALUES 
  ('Tackle', 35, 4),
  ('Water Gun', 40, 3),
  ('Ember', 40, 2),
  ('Vine Whip', 40, 1),
  ('Wing Attack', 65, 5),
  ('Headbutt', 70, 4),
  ('Return', 100, 4);

-- Pokemon_Move Table
INSERT INTO Pokemon_Move (pokemon_id, move_id)
VALUES 
  (1, 1), (1, 4), (1, 7),
  (2, 1), (2, 3), (2, 7),
  (3, 1), (3, 2), (3, 7),
  (4, 1), (4, 6), (4, 7),
  (5, 1), (5, 5), (5, 7);

-- Type_Relationship Table
INSERT INTO Type_Relationship (attacker_type_id, defender_type_id, effectiveness)
VALUES 
  (2, 1, 2), (1, 3, 2), (3, 2, 2),
  (4, 1, 1), (4, 2, 1), (4, 3, 1), (4, 5, 1),
  (1, 4, 1), (2, 4, 1), (3, 4, 1),
  (5, 1, 2), (5, 4, 1);
